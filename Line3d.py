class Line3d:
    """ 
    This is a class for describing a 3d line. 
      
    Attributes: 
        _theta (float): Angle to x axis.
        _alpha (float): Angle to y axis.
        _origin (Dot3d): Origin coordinates.
        _length (float): Length of the line.
    """
    def __init__(self, theta, alpha, origin, length):
        """ 
        Constructor for Dot3d class. 
  
        Parameters: 
            theta (float): Angle to x axis.
            alpha (float): Angle to y axis.
            origin (Dot3d): Origin coordinates as a Dot3d object.
            length (float): Length of the line.  
        """
        self._theta = theta
        self._alpha = alpha
        self._origin = origin
        self._length = length

    def get_theta(self):
        """ 
        Get the angle to x axis. 
    
        Returns: 
            float: Angle to x axis.
    
        """
        return self._theta

    def set_theta(self, theta):
        """ 
        Set the angle to x axis. 
    
        Parameters: 
            theta (float): The angle to x axis.
    
        """
        self._theta = theta
        
    def get_alpha(self):
        """ 
        Get the angle to y axis. 
    
        Returns: 
            float: Angle to y axis.
    
        """
        return self._alpha

    def set_alpha(self, alpha):
        """ 
        Set the angle to y axis. 
    
        Parameters: 
            alpha (float): The angle to y axis.
    
        """
        self._alpha = alpha
        
    def get_origin(self):
        """ 
        Get the origin coordinates as a Dot3d object.
    
        Returns: 
            float: Origin coordinates as a Dot3d object.
    
        """
        return self._origin

    def set_origin(self, origin):
        """ 
        Set the origin coordinates as a Dot3d object.
    
        Parameters: 
            origin (Dot3d): The origin coordinates as a Dot3d object.
    
        """
        self._origin = origin

    def get_length(self):
        """ 
        Get the length of the line.
    
        Returns: 
            float: The length of the line.
    
        """
        return self._length

    def set_length(self, length):
        """ 
        Set the length of the line.
    
        Parameters: 
            length (float): The length of the line.
    
        """
        self._length = length
    
    def __str__(self):
        """ 
        String representaion of a line. 
    
        Returns: 
            String: String representaion of a line.

        """
        return 'Origin is <{0}>, theta={1}, alpha={2}, length={3}'.format(self.get_origin(), 
                                                                        self.get_theta(), 
                                                                        self.get_alpha(), 
                                                                        self.get_length())
