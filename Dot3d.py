import math
from Line3d import Line3d
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as plt3d
import numpy

class Dot3d:
    """ 
    This is a class for describing a 3d dot. 
      
    Attributes: 
        _x (int): x coordinate.
        _y (int): y coordinate.
        _z (int): z coordinate.
    """
    def __init__(self, x, y, z): 
        """ 
        Constructor for Dot3d class. 
  
        Parameters: 
            x (int): x coordinate.
            y (int): y coordinate.
            z (int): z coordinate.    
        """
        self._x = x
        self._y = y
        self._z = z

    def get_x(self):
        """ 
        Get the value of x coordinate. 
    
        Returns: 
            int: The x coordinate value.
    
        """
        return self._x

    def set_x(self, x):
        """ 
        Set the value of x coordinate. 
    
        Parameters: 
            x (int): The x coordinate value.
    
        """
        self._x = x

    def get_y(self):
        """ 
        Get the value of y coordinate. 
    
        Returns: 
            int: The y coordinate value.
    
        """
        return self._y

    def set_y(self, y):
        """ 
        Set the value of y coordinate. 
    
        Parameters: 
            y (int): The y coordinate value.
    
        """
        self._y = y

    def get_z(self):
        """ 
        Get the value of z coordinate. 
    
        Returns: 
            int: The z coordinate value.
    
        """
        return self._z

    def set_z(self, z):
        """ 
        Set the value of z coordinate. 
    
        Parameters: 
            z (int): The z coordinate value.
    
        """
        self._z = z

    def get_line_between(self, dot3d):
        """ 
        Compute the line between this dot and another one. 
    
        Parameters: 
            dot3d (Dot3d): The end of line coordinates. 
    
        Returns: 
            Line3d: Line between the two dots. 
    
        """
        dx = self.get_x() - dot3d.get_x()
        dy = self.get_y() - dot3d.get_y()
        dz = self.get_z() - dot3d.get_z()
        length = math.sqrt(dx**2 + dy**2 + dz**2)
        if dy != 0:
            theta = Dot3d.degrees(math.atan(dx/dy))
        else:
            theta = 90
        if dz != 0:
            alpha = Dot3d.degrees(math.atan(dx/dz))
        else:
            alpha = 90
        return Line3d(theta, alpha, self, length)

    def draw_dots(dots):
        """ 
        Draw dots with lines between them. 
    
        Parameters: 
            dots (numpy.ndarray): Coordinates of all the dots.
    
        """
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.set_xlabel('X axis')
        ax.set_ylabel('Y axis')
        ax.set_zlabel('Z axis')

        xs = []
        ys = []
        zs = []
        prev_dot = None
        for x, y, z in dots:
            dot3d = Dot3d(x, y, z)
            if prev_dot != None:
                print(prev_dot.get_line_between(dot3d))
            ax.scatter(x, y, z, color='black', marker='o')
            xs.append(x)
            ys.append(y)
            zs.append(z)
            prev_dot = dot3d
            
        line = plt3d.art3d.Line3D(xs, ys, zs)
        ax.add_line(line)
        plt.show()

    def degrees(rads):
        """ 
        Convert from radians to degrees. 
    
        Parameters: 
            rads (float): Angle in radians.
    
        Returns: 
            float: Angle in degrees.
    
        """
        return 180 * rads/math.pi
  
    def __str__(self):
        """ 
        String representaion of a dot. 
    
        Returns: 
            String: String representaion of a dot.

        """
        return 'x={0}, y={1}, z={2}'.format(self.get_x(), self.get_y(), self.get_z())



if __name__ == "__main__":
    dots = numpy.array([
                [0, 0, 0],
                [3, 2, 0],
                [3, 2, 3],
                [5, 5, 5],
                [5, 6, 7],
                [7, 8, 9],
                [0, 10, 10]])
    Dot3d.draw_dots(dots)